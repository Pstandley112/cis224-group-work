var customName = document.getElementById("customname");
var randomize = document.querySelector(".randomize");
var story = document.querySelector(".story");

function randomValueFromArray(array){
	return array[Math.floor(Math.random()*array.length)];
}// randomizes array contents

var storyText = "It was 94 fahrenheit outside, so :insertx: went for a walk. When they got to :inserty:, they stared in horror for a few moments, then :insertz:. Silent Bob saw the whole thing, but was not surprised - :insertx: weighs 300 pounds, and it was a hot day.";
var insertX = ["Darth Vader", "T-1000", "Large Marge"];
var insertY = ["the Death Star", "the boiler room", "Chernobyl"];
var insertZ = ["bled acid through the floor", "got body-slammed by John Cena", "kicked eachother in the nards"];

randomize.addEventListener("click", result); //executes main function on the press of a button

function result() {
	var newStory = storyText;
	var xItem = randomValueFromArray(insertX);
	var yItem = randomValueFromArray(insertY);
	var zItem = randomValueFromArray(insertZ);

	newStory = newStory.replace(":insertx:", xItem);
	newStory = newStory.replace(":insertx:", xItem);
	newStory = newStory.replace(":inserty:", yItem);
	newStory = newStory.replace(":insertz:", zItem);

	if(customName.value !== "") {
    		var name = customName.value;
    		newStory = newStory.replace('Silent Bob',name);
  	} //replaces default name with custom name

	if(document.getElementById("uk").checked) {
		var weight = Math.round(300 * 0.0714286) + " stone";
		var temperature = Math.round(94-32) * 5 / 9 + " centigrade";
		newStory = newStory.replace("94 farenheit", temperature);
		newStory = newStory.replace("300 pounds", weight);
	} //checks for us or uk formulas

story.textContent = newStory;
story.style.visibility = "visible";
} //generates a random, silly story