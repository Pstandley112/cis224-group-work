var clickCount = 0;
var matches, $matches;
var monkeyClasses = ["SeeNoEvil", "HearNoEvil", "SpeakNoEvil"];
var $monkeys;
var pair = [];
var $resetBtn;

//document.ready function in jQuery $();
$(function() {
    $monkeys = $("#monkeys div");
    $matches = $("#matches");
    matches = Number($matches.text());
    $resetBtn = $("#reset");
    
    $monkeys.each(function() {
        let randomClass;
        let num = monkeyClasses.length;
        
        randomClass = monkeyClasses[
            Math.floor(Math.random() * num) 
        ];
        
        $(this).addClass("_" + randomClass);
    });
    
    function checkForMatch() {
        let classVal = $(this).attr("class");
        //console.log(classVal[0]);
        function updateClass(div) {
            if (classVal[0] === "_") {
                classVal = classVal.slice(1); 
                div.attr("class", classVal); 
                pair.push(classVal); 
                clickCount++;
            }
        }
        
        switch(clickCount) {
            case 0: 
            case 1:
                updateClass($(this));
                break;
                
            case 2:
                if (pair[0] === pair[1]) {matches++; $matches.text(matches);}
                break;
            
            default:
                console.log("Error");
        }
        
    }
    
    function resetGame() {
        $monkeys.each(function(){
        $(this).removeClass();
        });
                                 
        $matches.text(0);
        pair = [];
    }
    
    $monkeys.on("click", checkForMatch);
    $resetBtn.on("click", resetGame);
    
});